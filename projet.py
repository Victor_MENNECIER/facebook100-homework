# -*- coding: utf-8 -*-
"""
@author: Mennecier Victor
"""
import matplotlib.pyplot as plt
import networkx as nx
import pandas as pd
import numpy as np
import os



Caltech = nx.read_graphml('fb100\Caltech36.graphml')
MIT = nx.read_graphml('fb100\MIT8.graphml')
Johns = nx.read_graphml('fb100\Johns Hopkins55.graphml')





# Graphs = []
# j=0
# for i in os.listdir('fb100\\'):
#     print(i)
#     Graphs.append(nx.read_graphml('fb100\\' + i))
#     j+=1
#     print(str(j) + "%")






def degree_distrib(G):
    nb = nx.number_of_nodes(G)
    
    node_degree = []
    for i in range(nb):
        node_degree.append(nx.degree(G,str(i)))
    
    max_degree = max(node_degree)
    
    x = np.arange(max_degree)
    y = np.full(max_degree, None)
    
    for i in node_degree:
        if y[i-1] == None:
            y[i-1] = 0
        y[i-1] +=1
    
    x = np.array(x)
    y = np.array(y)
    plt.scatter(x,y,s=2)
    plt.show()
    

def adj_matrix(G):
    adj_matrix = (np.array(nx.adjacency_matrix(G).todense()))/2
    return adj_matrix
    
    
def global_clustering(G):
    A = adj_matrix(G)
    trace_cube = np.trace(np.linalg.matrix_power(A, 3))
    trace_carre = np.trace(np.linalg.matrix_power(A, 2))
    somme = np.sum(np.linalg.matrix_power(A, 2))
    
    return trace_cube / (somme - trace_carre)



def local_clustering(G,i):
    neighbors = np.array(G[str(i)])
    k = len(neighbors)
    if k == 1:
        return 0
    links_neighbors = 0
    for i in neighbors:
        for j in G[str(i)]:
            if j in neighbors:
                links_neighbors +=1

    return links_neighbors/( k*k-k )


def mean_local_clustering(G):
    N = G.number_of_nodes()
    somme = 0
    for i in range(N):
        somme += local_clustering(G,i)
    return somme / N


def edge_density(G):
    A = adj_matrix(G)
    m = np.sum(A)
    n = G.number_of_nodes()
    return m/(n*n-n)


def degree_vs_local_clustering(G):
    n = G.number_of_nodes()
    x = []
    y = []
    for i in range(n):
        x.append(nx.degree(G,str(i)))
        y.append(local_clustering(G, i))
    plt.scatter(x,y,s=2)
    plt.show()


def assortativity_distrib(G,attribute):
    x = []
    for graph in G:
        x.append(nx.attribute_assortativity_coefficient(graph, attribute=attribute))
    plt.hist(x)
    return


def assortativity_vs_size(G,attribute):
    x = []
    y = []
    for graph in G:
        y.append(nx.attribute_assortativity_coefficient(graph, attribute=attribute))
        x.append(graph.number_of_nodes())
    plt.scatter(x,y,s=2)
    return

def assortativity(attribute):
    distrib = []
    x = []
    y = []
    j=0
    for i in os.listdir('fb100\\'):
        graph = nx.read_graphml('fb100\\' + i)
        distrib.append(nx.attribute_assortativity_coefficient(graph, attribute=attribute))
        y.append(nx.attribute_assortativity_coefficient(graph, attribute=attribute))
        x.append(graph.number_of_nodes())
        j+=1
        print(str(j) + "%")
    
    plt.hist(distrib)
    plt.show()
    plt.scatter(x,y,s=2)
    plt.xscale('log')
    plt.show()






